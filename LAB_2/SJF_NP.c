#include <stdio.h>

struct Process
{
       int PNo, AT, BT, WT, TAT, CT;
};

void drawChart(struct Process P[], int a[], int N)
{
       int i, x = 0;
       printf("Gantt Chart\n");
       printf("---------------------------------------------\n");

       if (P[a[0]].AT != 0)
       {
              printf("0    %d |", P[a[0]].AT);
       }
       else
              printf("0");
       for (i = 0; i < N; i++)
       {
              printf("   [P%d]   %d |", P[a[i]].PNo, P[a[i]].CT);
       }
       printf("\n---------------------------------------------\n");

       return;
}

int main()
{
       int i, j, N, t = 0, a[100], minProcess, minTime = 999;
       struct Process P[100];
       float AvgWT = 0, AvgTAT = 0;

       printf("Enter Number of Processes\n");
       scanf("%d", &N);
       for (i = 0; i < N; i++)
       {
              printf("Enter Arrival Time & Burst Time for Process P%d\n", i + 1);
              scanf("%d %d", &P[i].AT, &P[i].BT);
              P[i].CT = 0;
              P[i].PNo = i + 1;
       }

       for (j = 0; j < N; j++)
       {
              if (minTime > P[j].AT)
              {
                     minTime = P[j].AT;
                     minProcess = j;
              }
       }
       t = P[minProcess].AT - 0;

       for (i = 0; i < N; i++)
       {
              minTime = 999;
              for (j = 0; j < N; j++)
              {
                     if ((P[j].AT <= t) && (P[j].CT == 0))
                     {
                            if (minTime == P[j].BT)
                            {
                                   if(P[j].AT<P[minProcess].AT)
                                   { minTime = P[j].BT;
                                   minProcess = j;}
                            }
                            else if (minTime > P[j].BT)
                            {
                                   minTime = P[j].BT;
                                   minProcess = j;
                            }
                     }
              }
              P[minProcess].CT = t + P[minProcess].BT;
              P[minProcess].TAT = P[minProcess].CT - P[minProcess].AT;
              P[minProcess].WT = P[minProcess].TAT - P[minProcess].BT;
              t = t + P[minProcess].BT;
              AvgWT = AvgWT + P[minProcess].WT;
              AvgTAT = AvgTAT + P[minProcess].TAT;
              a[i] = minProcess;
       }

       printf("\n-------------------------------------------\n");
       printf(" P.No\tAT\tBT\tCT\tTAT\tWT\n");
       printf("-------------------------------------------\n");
       for (i = 0; i < N; i++)
       {
              printf("  %d\t%d\t%d\t%d\t%d\t%d\n", P[i].PNo, P[i].AT, P[i].BT, P[i].CT, P[i].TAT, P[i].WT);
       }
       printf("___________________________________________\n\n");

       drawChart(P, a, N);
       AvgWT = AvgWT / N;
       AvgTAT = AvgTAT / N;
       printf("\nAverage Waiting Time : %.2f\n", AvgWT);
       printf("Average Turnaround Time : %.2f\n", AvgTAT);

       return 0;
}