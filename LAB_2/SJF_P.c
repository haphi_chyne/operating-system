#include <stdio.h>

struct Process
{
    int PNo, AT, BT, WT, TAT, CT;
};

int main()
{
    int i, j, N, t = 0, noP, a[100], burst[100], minProcess, minTime = 999;
    struct Process P[100];
    float AvgWT = 0, AvgTAT = 0;

    printf("Enter Number of Processes\n");
    scanf("%d", &N);
    noP = N;
    for (i = 0; i < N; i++)
    {
        printf("Enter Arrival Time & Burst Time for Process P%d\n", i + 1);
        scanf("%d %d", &P[i].AT, &P[i].BT);
        P[i].CT = 0;
        P[i].PNo = i + 1;
        burst[i] = P[i].BT; //Duplicate burst time
    }

    for (j = 0; j < N; j++)
    {
        if (minTime > P[j].AT)
        {
            minTime = P[j].AT;
            minProcess = j;
        }
    }
    t = P[minProcess].AT - 0;
    i = 0;

    printf("\n\nGantt Chart\n");
    printf("------------------------------------------------------------------------------------------\n");

    if (t != 0)
    {
        printf("0   %d |", t);
    }
    else
        printf("0");

    while (1)
    {
        minTime = 999;
        for (j = 0; j < N; j++)
        {
            if ((P[j].AT <= t) && (P[j].CT == 0))
            {
                if (minTime > P[j].BT)
                {
                    minTime = burst[j];
                    minProcess = j;
                }
            }
        }
        burst[minProcess]--;
        t++;
        a[i] = minProcess;
        printf(" [P%d] %d|", P[a[i]].PNo, t);
        if (burst[minProcess] == 0)
        {
            P[minProcess].CT = t;
            noP--;
        }
        if (noP == 0)
            break;
        i++;
    }
    printf("\n------------------------------------------------------------------------------------------\n");

    for (i = 0; i < N; i++)
    {
        P[i].TAT = P[i].CT - P[i].AT;
        P[i].WT = P[i].TAT - P[i].BT;
        AvgWT = AvgWT + P[i].WT;
        AvgTAT = AvgTAT + P[i].TAT;
    }

    printf("\n-------------------------------------------\n");
    printf(" P.No\tAT\tBT\tCT\tTAT\tWT\n");
    printf("-------------------------------------------\n");
    for (i = 0; i < N; i++)
    {
        printf("  %d\t%d\t%d\t%d\t%d\t%d\n", P[i].PNo, P[i].AT, P[i].BT, P[i].CT, P[i].TAT, P[i].WT);
    }
    printf("___________________________________________\n\n");

    AvgWT = AvgWT / N;
    AvgTAT = AvgTAT / N;
    printf("\nAverage Waiting Time : %.2f\n", AvgWT);
    printf("Average Turnaround Time : %.2f\n", AvgTAT);

    return 0;
}