#include <stdio.h>

struct Process
{
       int PNo, AT, BT, Priority, WT, TAT, CT;
};

void drawChart(struct Process P[], int a[], int N)
{
       int i, x = 0;
       printf("Gantt Chart\n");
       printf("---------------------------------------------\n");

       if (P[a[0]].AT != 0)
       {
              printf("0    %d |", P[a[0]].AT);
       }
       else
              printf("0");
       for (i = 0; i < N; i++)
       {
              printf("   [P%d]   %d |", P[a[i]].PNo, P[a[i]].CT);
       }
       printf("\n---------------------------------------------\n");

       return;
}

int main()
{
       int i, j, N, t = 0, a[100], currProcess, maxPriority=0, minTime = 999;
       struct Process P[100];
       float AvgWT = 0, AvgTAT = 0;

       printf("Enter Number of Processes\n");
       scanf("%d", &N);
       for (i = 0; i < N; i++)
       {
         printf("Enter Arrival Time, Burst Time and Priority for Process P%d\n", i + 1);
           scanf("%d %d %d", &P[i].AT, &P[i].BT, &P[i].Priority);
              P[i].CT = 0;
              P[i].PNo = i + 1;
       }

       for (j = 0; j < N; j++)
       {
              if (minTime > P[j].AT)
              {
                     minTime = P[j].AT;
                     currProcess = j;
              }
       }
       t = P[currProcess].AT - 0;

       for (i = 0; i < N; i++)
       {
              maxPriority= 0;
              for (j = 0; j < N; j++)
              {
                     if ((P[j].AT <= t) && (P[j].CT == 0))
                     {
                            if (maxPriority == P[j].Priority)
                            {
                                   if(P[j].AT<P[currProcess].AT)
                                   { minTime = P[j].BT;
                                   currProcess = j;}
                            }
                            else if (maxPriority < P[j].Priority)
                            {
                                   maxPriority = P[j].Priority;
                                   currProcess = j;
                            }
                     }
              }
              P[currProcess].CT = t + P[currProcess].BT;
              P[currProcess].TAT = P[currProcess].CT - P[currProcess].AT;
              P[currProcess].WT = P[currProcess].TAT - P[currProcess].BT;
              t = t + P[currProcess].BT;
              AvgWT = AvgWT + P[currProcess].WT;
              AvgTAT = AvgTAT + P[currProcess].TAT;
              a[i] = currProcess;
       }

    printf("\n----------------------------------------------------\n");
       printf(" P.No\tAT\tBT  Priority\tCT\tTAT\tWT\n");
    printf("----------------------------------------------------\n");
    for (i = 0; i < N; i++)
    {
        printf("  %d\t%d\t%d\t%d\t%d\t%d\t%d\n", P[i].PNo, P[i].AT, P[i].BT, P[i].Priority, P[i].CT, P[i].TAT, P[i].WT);
    }
    printf("____________________________________________________\n\n");

       drawChart(P, a, N);
       AvgWT = AvgWT / N;
       AvgTAT = AvgTAT / N;
       printf("\nAverage Waiting Time : %.2f\n", AvgWT);
       printf("Average Turnaround Time : %.2f\n", AvgTAT);

       return 0;
}
