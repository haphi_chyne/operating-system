import java.util.Random;
import java.util.Scanner;

public class ProducerConsumerProblem{
    static final int N = 5;
    
    static Monitor mon = new Monitor();

    public static void main(String args[]) {

        Scanner in = new Scanner(System.in);
        int n;

        System.out.print("\n1.Produce\n2.Consume\n0.Exit");
        while (true) {

            System.out.print("\nchoice: ");

            n = in.nextInt();
            switch (n) {
                case 1:
                    producer p = new producer();
                    p.start();
                    break;
                case 2:
                    consumer c = new consumer();
                    c.start();
                    break;
                case 0:
                    return;
            }
                        try {
                Thread.sleep(800);

            } catch (InterruptedException ex) {
                // TODO: handle exception
            }
            mon.displayBuffer();

        }
    }

    static class producer extends Thread {
        public void run() { // run method contains the thread code
            int item;
            item = produce_item();
            System.out.println("Producer is waiting to produce");
            mon.insert(item);
            System.out.println("Produced item :" + item);

        }

        private int produce_item() {
            Random random = new Random();
            return random.nextInt(200);
        } // actually produce
    }

    static class consumer extends Thread {
        public void run() { // run method contains the thread code
            int item;
            System.out.println("Consumer is waiting to consume");
            item = mon.remove();
            System.out.println("Consumer consumed item: " + item);

            consume_item(item);

        }

        private void consume_item(int item) {
        } // actually consume
    }

    static class Monitor { // this is a monitor
        private int buffer[] = new int[N];
        private int count = 0; // counters and indices

        public synchronized void insert(int val) {
            if (count == N)
                go_to_sleep(); // if the buffer is full, go to sleep
            buffer[count] = val; // inser t an item into the buffer
            count = count + 1;
            // one more item in the buffer now
            if (count == 1)
                notify();
            // if consumer was sleeping, wake it up
        }

        public synchronized int remove() {
            int val;
            if (count == 0)
                go_to_sleep(); // if the buffer is empty, go to sleep
            val = buffer[count-1]; // fetch an item from the buffer
            count = count - 1;
            // one few items in the buffer
            if (count == N - 1)
                notify(); // if producer was sleeping, wake it up
            return val;
        }

        private void go_to_sleep() {
            try {
                wait();
            } catch (InterruptedException ex) {
                // TODO: handle exception
            }
        }

        public synchronized void displayBuffer() {
            int i;
            System.out.print("\nBuffer : ");
            for (i = 0; i < count; i++) {
                System.out.print(" " + buffer[i]);
            }
        }
    }
}

