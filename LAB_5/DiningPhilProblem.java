import java.util.Random;
import java.util.Scanner;

public class DiningPhilProblem {
    static final int N = 5;

    static Monitor monitor = new Monitor();

    // instantiate a new monitor
    public static void main(String args[]) {

        for (int i = 0; i < N; i++) {
            Philosopher philosopher = new Philosopher(i);
            philosopher.start();
        }

    }

    static class Philosopher extends Thread {
        private int i;

        public void run() { // run method contains the thread code
            while (true) {
                System.out.println("Philosopher " + (i + 1) + " is Thinking");
                try {
                    Thread.sleep(new Random().nextInt(1500));
                } catch (InterruptedException ex) {
                    // TODO: handle exception

                }
                monitor.take_fork(i);
                System.out.println("Philosopher " + (i + 1) + " is Eating");
                monitor.put_fork(i);
            }

        }

        Philosopher(int i) {
            this.i = i;
        }
    }

    static class Monitor { // this is a monitor
        static final int N = 5;

        enum State {
            THINKING, HUNGRY, EATING
        };

        State state[] = new State[5];

        public synchronized void take_fork(int n) {
            state[n] = State.HUNGRY;
            System.out.println("Philosopher " + (n + 1) + " is Hungry");

            test(n);

            if (state[n] != State.EATING)
                go_to_sleep();
        }

        public synchronized void put_fork(int n) {
            state[n] = State.THINKING;
            test((n + 4) % 5);
            test((n + 1) % 5);
        }

        public synchronized void test(int n) {
            if (state[n] == State.HUNGRY && state[(n + N - 1) % N] != State.EATING
                    && state[(n + 1) % N] != State.EATING) {
                state[n] = State.EATING;
                notify();
            }
        }

        private void go_to_sleep() {
            try {
                wait();
            } catch (InterruptedException ex) {
                // TODO: handle exception
            }
        }
    }
}

