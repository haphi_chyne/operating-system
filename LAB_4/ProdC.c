#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>

#define BUFFER_SIZE 5

pthread_mutex_t mutex;
pthread_t tid;
sem_t empty;
sem_t full;
int buffer[BUFFER_SIZE];
int counter;

void initializeData()
{

    /* Create the mutex lock */
    pthread_mutex_init(&mutex, NULL);

    /* Create the full semaphore and initialize to 0 */
    sem_init(&full, 0, 0);

    /* Create the empty semaphore and initialize to BUFFER_SIZE */
    sem_init(&empty, 0, BUFFER_SIZE);


    /* init buffer */
    counter = 0;
}

int insert_item(int item)
{
    /* When the buffer is not full add the item
      and increment the counter*/
    if (counter < BUFFER_SIZE)
    {
        buffer[counter] = item;
        counter++;
        return item;
    }
    else
    { /* Error the buffer is full */
        return -1;
    }
}

int remove_item()
{
    /* When the buffer is not empty remove the item
      and decrement the counter */
    int item;
    if (counter > 0)
    {
        item = buffer[counter - 1];
        counter--;
        return item;
    }
    else
    { /* Error buffer empty */
        return -1;
    }
}

void *producer(void *param)
{
    int item;
    item = 20 + rand() % (200 - 20 + 1);

    printf("\nproducer waiting to produce %d....",item);
    /* acquire the empty lock */
    sem_wait(&empty);
    /* acquire the mutex lock */
    pthread_mutex_lock(&mutex);

    printf("\nproducer produced %d", insert_item(item));
    /* release the mutex lock */
    pthread_mutex_unlock(&mutex);
    /* signal full */
    sem_post(&full);
}

void *consumer(void *param)
{
    printf("\nconsumer waiting to consume....");
    sem_wait(&full);
    pthread_mutex_lock(&mutex);

    printf("\nconsumer consumed %d", remove_item());

    pthread_mutex_unlock(&mutex);
    sem_post(&empty);
}

void displayBuffer()
{
    int i;
    printf("\nBuffer : ");
    for (i = 0; i < counter; i++)
    {
        printf("%d ", buffer[i]);
    }
}

int main()
{
    int n;

    initializeData();

    printf("\n1.Produce\n2.Consume\n0.Exit");
    while (1)
    {
        sleep(1);
        displayBuffer();
        printf("\n\n> ");
        scanf("%d", &n);
        switch (n)
        {
        case 1:
            pthread_create(&tid, NULL, producer, NULL);
            break;
        case 2:
            pthread_create(&tid, NULL, consumer, NULL);
            break;
        case 0:
            exit(0);
            break;
        }
    }

    exit(0);
}
