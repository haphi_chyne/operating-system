#include<stdio.h>

void Sort(int BurstTime[], int PNo[], int n);

int main()
{	
	int PNo[25], BurstTime[25], WaitingTime[25], Time=0, TotalWait=0, i,j,h, N;
	float AvgWaiting;
		
	printf("Number of Process: ");
	scanf("%d", &N);


	for(i=0; i<N; i++)
	{
	printf("Burst time for Process %d : ", i+1);
	scanf("%d", &BurstTime[i]);
	PNo[i]=i+1;
	}

	Sort(BurstTime, PNo, N);
	
	for(i=0;i<N;i++)
	{	
		WaitingTime[i]= Time; 
		Time+=BurstTime[i];
		TotalWait += WaitingTime[i]; 
	}
	

	AvgWaiting= TotalWait/N;

	printf("Gantt Chart:\n\nn");

	
	printf("[");
	printf("0");
	for(i=0;i<N;i++)
	{	
		h= (int)BurstTime[i]/2;

		for(j=0;j<(int)BurstTime[i]; j++)
		{
		
		if(j==h)
		printf("..P%d..", PNo[i]);

		else
		printf(".");

		}
	//	printf(" ");
	if(i!=N-1)
	printf("%d|", WaitingTime[i+1]);
	
	}
	printf("%d",Time);
	printf("]");

	printf("\n\nAverage Waiting Time: %f\n\n", AvgWaiting);



}


void Sort(int BurstTime[], int PNo[], int n) 
{ 
   int i, j, temp; 
   for (i = 0; i < n-1; i++)       
     
       for (j = 0; j < n-i-1; j++)  
           if (BurstTime[j] > BurstTime[j+1]) 
	{	
   	temp = BurstTime[j]; 
    	BurstTime[j] = BurstTime[j+1]; 
   	BurstTime[j+1] = temp; 

	temp = PNo[j]; 
    	PNo[j] = PNo[j+1]; 
   	PNo[j+1] = temp; 
        }
} 
