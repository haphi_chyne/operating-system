#include<stdio.h>

int main()
{
	int BurstTime[25], WaitingTime[25], Time=0, TotalWait=0, i,j,h, N;
	float AvgWaiting;
		
	printf("Number of Process: ");
	scanf("%d", &N);

	for(i=0; i<N; i++)
	{
	WaitingTime[i]= Time;

	printf("Burst time for Process %d : ", i+1);
	scanf("%d", &BurstTime[i]);

	Time+=BurstTime[i];
	}
	
	for(i=0;i<N;i++)
	{
	TotalWait += WaitingTime[i]; 
	}

	AvgWaiting= TotalWait/N;

	printf("Gantt Chart:\n");

	
	printf("[");
	printf("0");
	for(i=0;i<N;i++)
	{	
		h= (int)BurstTime[i]/2;

		for(j=0;j<(int)BurstTime[i]; j++)
		{
		
		if(j==h)
		printf("..P%d..", i+1);

		else
		printf(".");

		}

	if(i!=N-1)
	printf("%d|", WaitingTime[i+1]);
	
	}
	printf("%d",Time);
	printf("]");

	printf("\n\nAverage Waiting Time: %f", AvgWaiting);

}
