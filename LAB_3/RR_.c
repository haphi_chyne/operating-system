#include <stdio.h>

int front=-1, rear=-1;

typedef struct
{
    int PNo, AT, BT, WT, TAT, CT, RemBT;
} Process;

void sortAT(Process a[], int N)
{
    int i, j;
    Process temp;
    for (i = 0; i < N; i++)
    {
        for (j = i + 1; j < N; j++)
        {
            if (a[i].AT > a[j].AT)
            {
                temp = a[i];
                a[i] = a[j];
                a[j] = temp;
            }
        }
    }
    return;
}

void sortPNo(Process a[], int N)
{
    int i, j;
    Process temp;
    for (i = 0; i < N; i++)
    {
        for (j = i + 1; j < N; j++)
        {
            if (a[i].PNo > a[j].PNo)
            {
                temp = a[i];
                a[i] = a[j];
                a[j] = temp;
            }
        }
    }
    return;
}

void insert(int j, int Q[])
{
    if (rear == 100)
    printf("Queue Overflow \n");
    else
    {
        if (front == -1)
            front = 0;
        rear = rear + 1;
        Q[rear] = j;
    }
} 


void delete(int Q[])
{
    int j;
    if (front == - 1 || front > rear)
    {
        printf("Queue Underflow \n");
        return ;
    }
    else
    {
       for (j = 0; j < 100; j++)
        {
            Q[j]=Q[j+1];
        }
        rear--;
    }
}



int main()
{
    int i, j, N,QTime,time = 0, noP, a[100], minProcess, minTime = 999,Queue[100];
    Process P[100];
    float AvgWT = 0, AvgTAT = 0;

    printf("Enter Number of Processes\n");
    scanf("%d", &N);
    noP = N;
    printf("Enter Quantum Number : ");
    scanf("%d", &QTime);

    for (i = 0; i < N; i++)
    {
        printf("Enter Arrival Time & Burst Time for Process P%d\n", i + 1);
        scanf("%d %d", &P[i].AT, &P[i].BT);
        P[i].CT = 0;
        P[i].PNo = i + 1;
        P[i].RemBT = P[i].BT; //Duplicate remaining burst time
    }

    sortAT(P, N);

    printf("\n\nGantt Chart\n");
   printf("------------------------------------------------------------------------------------------------------------------------------------------------------\n");

    if (P[0].AT > 0)
    {
        printf("0   %d |", P[0].AT);
        time= P[0].AT;
    }
    else
        printf("0");

    
    for (j = 0; j < N; j++)
    {
        if ((P[j].AT <= time) && (P[j].CT == 0))
        {
            insert(j, Queue);
            P[j].CT = -1;  
        }
    }


    while (1)
    {
        if (P[Queue[front]].RemBT <= QTime && P[Queue[front]].RemBT > 0)
        {
            time = time + P[Queue[front]].RemBT;
            printf("  (P%d)  %d |",P[Queue[front]].PNo, time);
            P[Queue[front]].CT = time;
            P[Queue[front]].RemBT = 0;
            noP--;
        }
        else if (P[Queue[front]].RemBT > 0)
        {
            P[Queue[front]].RemBT = P[Queue[front]].RemBT - QTime;
            time = time + QTime;
            printf("  (P%d)  %d |", P[Queue[front]].PNo, time);     
        }  
        for (j = 0; j < N; j++)
        {
            if((P[j].AT <= time) && (P[j].CT == 0))
            {
                insert(j,Queue);
                P[j].CT = -1;
            }
        }
        insert(Queue[front],Queue);
        delete(Queue); 

        if (noP == 0)
            break;
    }
  printf("\n------------------------------------------------------------------------------------------------------------------------------------------------------\n");


    for (i = 0; i < N; i++)
    {
        P[i].TAT = P[i].CT - P[i].AT;
        P[i].WT = P[i].TAT - P[i].BT;
        AvgWT = AvgWT + P[i].WT;
        AvgTAT = AvgTAT + P[i].TAT;
    }
    sortPNo(P,N);

    printf("\n-------------------------------------------\n");
    printf(" P.No\tAT\tBT\tCT\tTAT\tWT\n");
    printf("-------------------------------------------\n");
    for (i = 0; i < N; i++)
    {
        printf("  %d\t%d\t%d\t%d\t%d\t%d\n", P[i].PNo, P[i].AT, P[i].BT, P[i].CT, P[i].TAT, P[i].WT);
    }
    printf("___________________________________________\n\n");

    AvgWT = AvgWT / N;
    AvgTAT = AvgTAT / N;
    printf("\nAverage Waiting Time : %.2f\n", AvgWT);
    printf("Average Turnaround Time : %.2f\n", AvgTAT);

    return 0;
}
